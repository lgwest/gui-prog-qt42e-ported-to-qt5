import sys
from PySide2.QtWidgets import *

app = QApplication(sys.argv)
label = QPushButton("Quit")
label.clicked.connect(app.quit)
label.show()
exit(app.exec_())
