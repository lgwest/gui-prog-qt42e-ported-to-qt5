# Hello World example: the basic example,
# showing how to connect a signal to a slot without any parameters
import sys
from PySide2 import QtWidgets


# define a function that will be used as a slot
def sayHello():
    print('Hello world!')


app = QtWidgets.QApplication(sys.argv)

button = QtWidgets.QPushButton('Say hello!')

# connect the clicked signal to the sayHello slot
button.clicked.connect(sayHello)
button.show()

sys.exit(app.exec_())
