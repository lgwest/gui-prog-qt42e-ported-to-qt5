from PySide2.QtWidgets import *
from PySide2.QtCore import *

class FindDialog(QDialog, QObject):
    findNext = Signal()
    findPrevious = Signal()

    def __init__(self):
        super().__init__()

        self.label = QLabel('Find &what')
        self.lineEdit = QLineEdit()
        self.label.setBuddy(self.lineEdit)

        self.caseCheckBox = QCheckBox('Match &case')
        self.backwardCheckBox = QCheckBox('Search &backwards')

        self.findButton = QPushButton('&Find')
        self.findButton.setDefault(True)
        self.findButton.setEnabled(False)

        self.closeButton = QPushButton('&Close')

        self.lineEdit.textChanged.connect(self.evt_enableFindButton(str))
        self.findButton.clicked.connect(self.evt_findClicked())
        self.closeButton.clicked.connect(self.close)

        self.topLeftLayout = QHBoxLayout()
        self.topLeftLayout.addWidget(self.label)
        self.topLeftLayout.addWidget(QLineEdit)

        self.leftLayout = QVBoxLayout()
        self.leftLayout.addLayout(self.topLeftLayout)
        self.leftLayout.addWidget(self.caseCheckBox)
        self.leftLayout.addWidget(self.backwardCheckBox)

        self.rightLayout = QVBoxLayout()
        self.rightLayout.addWidget(self.self.findButton)
        self.rightLayout.addWidget(self.closeButton)
        self.rightLayout.addStretch()

        self.mainLayout = QHBoxLayout()
        self.mainLayout.addLayout(self.leftLayout)
        self.mainLayout.addLayout(self.rightLayout)

        self.setLayout(self.mainLayout)

        self.setWindowTitle("Find")
        self.setFixedHeight(self.sizeHint().height())

    def evt_findClicked(self):
        text = self.lineEdit.text()
        cs = Qt.CaseSensitive if self.caseCheckBox.isChecked() else Qt.CaseInsensitive
        if self.backwardCheckBox.isChecked():
            self.findPrevious(text, cs)
        else:
            self.findNext(text, cs)

    def evt_enableFindButton(self, text):
        self.findButton.setEnabled(len(text) > 0)


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    dialog = FindDialog()
    dialog.show()
    exit(app.exec_())
